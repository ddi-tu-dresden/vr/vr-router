﻿using System.Collections.Generic;

internal class packageStatistic
{
    /// <summary>
    /// Number of the Package during the scripted Study-Run
    /// </summary>
    public int Number;

    /// <summary>
    /// Time from first spawning of a Package to correct assignment
    /// </summary>
    public double Time;

    /// <summary>
    /// Number of Tubes that where usesd as wrong assignment in the order of assignment
    /// </summary>
    public List<int> Fails;
}