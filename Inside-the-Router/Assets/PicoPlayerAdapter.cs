using System.Collections;
using System.Collections.Generic;
using OmiLAXR;
using OmiLAXR.Adapters;
using OmiLAXR.Adapters.UnityXR;
using OmiLAXR.Interaction;
using Unity.XR.CoreUtils;
using UnityEngine;

public class PicoPlayerAdapter : MonoBehaviour, IPlayer
{
    public GameObject XR_Rig;

    public Vector3 WorldPosition => XR_Rig.transform.position;

    public Vector3 WorldRotation => XR_Rig.transform.rotation.eulerAngles;

    public bool IsVR => true;

    public event ActionChangedHandler OnActionChanged;

    public Transform GetBody() => XR_Rig.transform;

    public Camera GetCamera() => Camera.main;

    public string[] GetControllerActions() => new string[] { "Grab", "Throw", "UI Press" };

    public Transform GetHead() => Camera.main.transform;

    public Transform GetTransform() => Camera.main.transform;

    public GameObject GetLeftHand() => XR_Rig.transform.Find("LeftHandController")?.gameObject;

    public GameObject GetRightHand() => XR_Rig.transform.Find("RightHandController")?.gameObject;

    void Start()
    {
        Learner.Instance.AssignPlayer(this);
    }

    public ILaserPointer GetLeftPointer()
    {
        throw new System.NotImplementedException();
    }

    public ILaserPointer GetRightPointer()
    {
        throw new System.NotImplementedException();
    }
}
