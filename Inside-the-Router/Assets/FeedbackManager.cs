using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

public class FeedbackManager : MonoBehaviour
{
    public int ErrorThreshold = 3;
    public float TimeThreshold = 10;
    public bool NotifyTubes = false;
    public bool NotifyPackages = true;
    public bool NotifyNatTable = true;
    public bool isFeedbackCurrentlyActive { get; private set; } = false;

    private List<FeedbackChannel> feedbackChannels = new List<FeedbackChannel>();
    
    private int errorCounter = 0;
    private float timeCounter = 0;

    public void RegisterFeedbackChannel(FeedbackChannel fg)
    {
        if (feedbackChannels.Contains(fg)) return;

        feedbackChannels.Add(fg);
    }

    public void UnregisterFeedbackChannel(FeedbackChannel fg)
    {
        if (feedbackChannels.Contains(fg))
        {
            feedbackChannels.Remove(fg);
        }
    }

    private void Start()
    {
        SubscribeToRunEvents();
    }

    private void OnDestroy()
    {
        UnsubscribeFromRunEvents();
    }

    private void SubscribeToRunEvents()
    {
        Run.CorrectPackagePlacement += ResetCounters;
        Run.RunStarted += ResetCounters;
        Run.WrongPackagePlacement += IncreaseTriesCounter;
        Run.RunFinished += OnRunFinished;
        
    }

    private void UnsubscribeFromRunEvents()
    {
        Run.CorrectPackagePlacement -= ResetCounters;
        Run.RunStarted -= ResetCounters;
        Run.WrongPackagePlacement -= IncreaseTriesCounter;
        Run.RunFinished -= OnRunFinished;
    }

    private void OnRunFinished(object sender, RunEventArgs e)
    {
        isFeedbackCurrentlyActive = false;
    }

    private void ResetCounters(object sender, RunEventArgs e)
    {
        timeCounter = 0;
        errorCounter = 0;
        isFeedbackCurrentlyActive = false;
    }

    private void IncreaseTriesCounter(object sender, RunEventArgs e)
    {
        errorCounter++;
    }

    private void RequestFeedback(FeedbackCase feedbackCase)
    {
        foreach(FeedbackChannel fc in feedbackChannels)
        {
            if (fc is TubeScript && NotifyTubes)
            {
                fc.OnFeedbackRequested(feedbackCase);
            }

            if (fc is PackageScript && NotifyPackages)
            {
                fc.OnFeedbackRequested(feedbackCase);
            }

            if (fc is GameDisplayScript && NotifyNatTable)
            {
                fc.OnFeedbackRequested(feedbackCase);
            }
        }
    }

    
    private void Update()
    {
        timeCounter += Time.deltaTime;

        if (AreFeedbackConditionsMet())
        {
            //TODO: Determine Feedback Case
            RequestFeedback(FeedbackCase.UNKNOWN);
            isFeedbackCurrentlyActive = true;
        }
    }

    private bool AreFeedbackConditionsMet()
    {
        bool conditions = timeCounter >= TimeThreshold || errorCounter >= ErrorThreshold;

        return !isFeedbackCurrentlyActive && conditions;
    }

    private FeedbackCase DetermineFeedbackCase()
    {
        throw new NotImplementedException();

        /*
        Package p = null;

        if(RunController.CurrentRunController.CurrentPackages.Count > 0)
        {
            p = RunController.CurrentRunController.CurrentPackages[0].GetDataPackage();
        }

        // Type Conditions:
        // 1 - Internal
        // 2 - Outgoing
        // 3 - Incoming
        int typeCondition = 2;

        if(Run.CurrentRun.Tubes.Any(t => t.IpAddress == p.Destination.IpAddress))
        {
            typeCondition = 1;
        }
        else if(NatTable.Entries.Any(n => n.ExternalAddress == p.Destination))
        {
            typeCondition = 3;
        }

        // TriggerCondition
        // Tries - 1
        // Time - 2
        int triggerCondition = 2;
        if (errorCounter > ErrorThreshold) triggerCondition = 1;


        return (FeedbackCase)(typeCondition * triggerCondition);
        */
    }
}
