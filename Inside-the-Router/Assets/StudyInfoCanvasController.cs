using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StudyInfoCanvasController : MonoBehaviour
{
    private TextMeshProUGUI StudyCodeText;

    private const string CodeKey = "HMD-Code";

    // Start is called before the first frame update
    void Start()
    {
        StudyCodeText = gameObject.GetComponent<TextMeshProUGUI>();

        if (PlayerPrefs.HasKey(CodeKey))
        {
            StudyCodeText.text = "Brillen-Code: " + PlayerPrefs.GetString(CodeKey);
        }
        else
        {
            string ValidChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ123456789";
            string code = string.Empty;
            UnityEngine.Random.InitState((int)DateTime.Now.Ticks);
            for (int i = 0; i < 4; i++)
            {
                code += ValidChars[UnityEngine.Random.Range(0, ValidChars.Length)];
            }

            PlayerPrefs.SetString(CodeKey, code);
            StudyCodeText.text = "Brillen-Code: " + code;
        }
    }
}
