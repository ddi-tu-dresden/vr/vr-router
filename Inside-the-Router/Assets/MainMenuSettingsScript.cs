using UnityEngine;
using UnityEngine.UI;

public class MainMenuSettingsScript : MonoBehaviour
{
    //Game Settings
    public MultipleButtonsSetting GameVersionSelector;
    public MultipleButtonsSetting PlayModeSelector;

    //General Settings
    public Slider VolumeSlider;
    public Slider MusicVolumeSlider;
    public MultipleButtonsSetting LanguageSelector;

    void Start()
    {
        GetSettingsFromGameManager();
        SubscribeEvents();
        UpdateScenePersistantContainer(0);
    }

    private void OnDestroy()
    {
        UnsubscribeEvents();
    }

    private void SubscribeEvents()
    {
        GameVersionSelector.OnValueChanged += UpdateScenePersistantContainer;
        PlayModeSelector.OnValueChanged += UpdateScenePersistantContainer;

        VolumeSlider.onValueChanged.AddListener(VolumeSliderUpdate);
        MusicVolumeSlider.onValueChanged.AddListener(MusicVolumeSliderUpdate);
        LanguageSelector.OnValueChanged += LanguageChanged;
    }

    private void UnsubscribeEvents()
    {
        GameVersionSelector.OnValueChanged -= UpdateScenePersistantContainer;
        PlayModeSelector.OnValueChanged -= UpdateScenePersistantContainer;

        VolumeSlider.onValueChanged.RemoveAllListeners();
        MusicVolumeSlider.onValueChanged.RemoveAllListeners();
        LanguageSelector.OnValueChanged -= LanguageChanged;
    }

    private void UpdateScenePersistantContainer(int arg0)
    {
        Debug.Log("Game Version: " + GameVersionSelector.CurrentValue + " -> " + (StartArguments.Version)GameVersionSelector.CurrentValue);
        Debug.Log("Play Mode: " + PlayModeSelector.CurrentValue + " -> " + (StartArguments.LenghtMode)PlayModeSelector.CurrentValue);
        StartArguments.Instance.version = (StartArguments.Version)GameVersionSelector.CurrentValue;
        StartArguments.Instance.lenghtMode = (StartArguments.LenghtMode) PlayModeSelector.CurrentValue;
    }

    void VolumeSliderUpdate(float newValue)
    {
        Settings.Instance.Volume = newValue;
    }

    void MusicVolumeSliderUpdate(float newValue)
    {
        Settings.Instance.MusicVolume = newValue;
    }

    void LanguageChanged(int newValue)
    {
        Localization.Get.LoadLanguage(Localization.Get.SupportedLanguages[newValue]);
        Localization.Get.UpdateAllLocalization();
        Settings.Instance.Language = newValue;
    }

    public void GetSettingsFromGameManager()
    {
        VolumeSlider.value = Settings.Instance.Volume;
        MusicVolumeSlider.value = Settings.Instance.MusicVolume;


        LanguageSelector.ButtonClicked(Settings.Instance.Language);
        Localization.Get.LoadLanguage(Localization.Get.SupportedLanguages[LanguageSelector.CurrentValue]);
        Localization.Get.UpdateAllLocalization();
    }

#if DEBUG

    [ContextMenu("setLanguageGerman")]
    public void setLanguageGerman()
    {
        LanguageSelector.ButtonClicked(1);
    }

    [ContextMenu("setLanguageEnglish")]
    public void setLanguageEnglish()
    {
        LanguageSelector.ButtonClicked(0);
    }

#endif

}
