using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class SettingsPanel : MonoBehaviour
{
    public bool FollowPlayerView = true;
    public Transform SpawnPosition;

    public Button CloseButton;
    public Button BackToMenuButton;
    public MultipleButtonsSetting InstructionVisibility;
    public MultipleButtonsSetting AimAssist;
    public MultipleButtonsSetting Language;
    public Slider VolumeSlider;
    public Slider MusicVolumeSlider;
    public Slider SpeechVolumeSlider;
    public TMP_Text TimeText;

    float MinHeight = 0.5f;

    public ToggleInteractorMode toggleInteractorMode;

    public GameObject[] HideInMainMenu;

    private void Awake()
    {
        if (!FollowPlayerView) {
            foreach (GameObject onlyIngameButton in HideInMainMenu)
            {
                onlyIngameButton.SetActive(false);
            }
        }

        if(StartArguments.Instance != null)
        {
            LanguageChanged(StartArguments.Instance.Language);
        }
    }

    void Start()
    {
        CloseButton.onClick.AddListener(Close);
        BackToMenuButton.onClick.AddListener(BackToMenu);
        InstructionVisibility.OnValueChanged += InstructionVisibilityChanged;
        VolumeSlider.onValueChanged.AddListener(VolumeSliderUpdate);
        MusicVolumeSlider.onValueChanged.AddListener(MusicVolumeSliderUpdate);
        SpeechVolumeSlider.onValueChanged.AddListener(SpeechVolumeSliderUpdate);
        AimAssist.OnValueChanged += AimAssistChanged;
        Language.OnValueChanged += LanguageChanged;
        GetSettingsFromGameManager();
    }

    private void OnDestroy()
    {
        CloseButton.onClick.RemoveAllListeners();
        BackToMenuButton.onClick.RemoveAllListeners();
        InstructionVisibility.OnValueChanged -= InstructionVisibilityChanged;
        VolumeSlider.onValueChanged.RemoveAllListeners();
        MusicVolumeSlider.onValueChanged.RemoveAllListeners();
        SpeechVolumeSlider.onValueChanged.RemoveAllListeners();
        AimAssist.OnValueChanged -= AimAssistChanged;
        Language.OnValueChanged -= LanguageChanged;
    }

    void InstructionVisibilityChanged(int newValue) {
        Settings.Instance.InstructionVisibility = newValue == 0 ? Settings.TextMode.VISIBLE : Settings.TextMode.HIDDEN;
    }

    void VolumeSliderUpdate(float newValue) {
        Settings.Instance.Volume = newValue;
    }

    void MusicVolumeSliderUpdate(float newValue)
    {
        Settings.Instance.MusicVolume = newValue;
    }
    void SpeechVolumeSliderUpdate(float newValue)
    {
        Settings.Instance.SpeechVolume = newValue;
    }

    void AimAssistChanged(int newValue) { 
        Settings.Instance.AimAssist = newValue == 0 ? Settings.AimAssistMode.ON : Settings.AimAssistMode.OFF;
    }

    void LanguageChanged(int newValue) {
        Localization.Get.LoadLanguage(Localization.Get.SupportedLanguages[newValue]);
        Localization.Get.UpdateAllLocalization();
        Settings.Instance.Language = newValue;
    }

    float RotateLerpDistancePerSecond = 360f;

    private void Update()
    {
        if (RunController.CurrentRunController != null)
        {
            float passedTime = Time.time - RunController.CurrentRunController.StartTime;
            int minutes = Mathf.FloorToInt(passedTime / 60);
            int seconds = (int)passedTime % 60;
            TimeText.text = minutes + ":" + seconds.ToString("00");
        }
        else TimeText.text = "";

        if (SpawnPosition != null)
        {
            transform.position = new Vector3(SpawnPosition.position.x, Mathf.Max(MinHeight, SpawnPosition.position.y), SpawnPosition.position.z);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, SpawnPosition.rotation, RotateLerpDistancePerSecond * Time.deltaTime);
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0f);
        }
    }

    public void OnEnable()
    {
        if (SpawnPosition == null) return;
        transform.position = new Vector3(SpawnPosition.position.x, Mathf.Max(MinHeight, SpawnPosition.position.y), SpawnPosition.position.z);
        transform.rotation = SpawnPosition.rotation;
        transform.eulerAngles = new Vector3(0f, transform.eulerAngles.y, 0f);

        GetSettingsFromGameManager();
        toggleInteractorMode.Toggle(true);
    }

    private void OnDisable()
    {
        if (SpawnPosition == null) return;
        toggleInteractorMode.Toggle(false);
    }

    public void Close() {
        gameObject.SetActive(false);
    }

    public void BackToMenu() {
        SceneManager.LoadScene(0);
    }

    public void GetSettingsFromGameManager() {
        InstructionVisibility.ButtonClicked(Settings.Instance.InstructionVisibility == Settings.TextMode.VISIBLE ? 0 : 1);
        VolumeSlider.value = Settings.Instance.Volume;
        MusicVolumeSlider.value = Settings.Instance.MusicVolume;
        SpeechVolumeSlider.value = Settings.Instance.SpeechVolume;
        AimAssist.ButtonClicked(Settings.Instance.AimAssist == Settings.AimAssistMode.ON ? 0 : 1);
        Language.ButtonClicked(Settings.Instance.Language);
        Localization.Get.LoadLanguage(Localization.Get.SupportedLanguages[Language.CurrentValue]);
        Localization.Get.UpdateAllLocalization();
    }
}
