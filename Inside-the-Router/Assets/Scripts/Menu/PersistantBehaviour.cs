using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PersistantBehaviour : MonoBehaviour
{
    /// <summary>
    /// use this instead of start or awake. You can safely assume that you are on the correct persistent container.
    /// </summary>
    public abstract void SetUp();
}
