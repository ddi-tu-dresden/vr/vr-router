using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Settings : PersistantBehaviour
{
    public static Settings Instance;

    public AudioMixer MainMixer;

    public enum TextMode
    {
        HIDDEN, VISIBLE
    }

    public enum AimAssistMode
    {
        ON, OFF
    }

    //Serializing private field means we can edit them in the unity inspector, while still forcing other classes to use the getters/setters
    //defined below, if they want to change the values.
    [SerializeField] private TextMode instructionVisibility;
    [SerializeField] private AimAssistMode aimAssist;
    [SerializeField] private float volume;
    [SerializeField] private float musicVolume;
    [SerializeField] private float speechVolume;
    [SerializeField] private int language;

    public override void SetUp()
    {
        Instance = this;
        OnSettingChange += UpdateSettingsGlobal;
    }

    private void OnDestroy()
    {
        OnSettingChange -= UpdateSettingsGlobal;
    }

    public void UpdateSettingsGlobal() {
        MainMixer.SetFloat("MainVolume", FloatToVolume(volume));
        MainMixer.SetFloat("MusicVolume", FloatToVolume(musicVolume));
        MainMixer.SetFloat("SpeechVolume", FloatToVolume(speechVolume));
    }

    public float FloatToVolume(float f) {
        f = f * 60f - 40f;
        if (f < -39.9f) f = -80f;
        return f;
    }

    public Action OnSettingChange;

    public TextMode InstructionVisibility { get => instructionVisibility; set { instructionVisibility = value; OnSettingChange?.Invoke(); } }
    public AimAssistMode AimAssist { get => aimAssist; set { aimAssist = value; OnSettingChange?.Invoke(); } }
    public float Volume { get => volume; set { volume = value; OnSettingChange?.Invoke(); } }
    public float MusicVolume { get => musicVolume; set { musicVolume = value; OnSettingChange?.Invoke(); } }
    public float SpeechVolume { get => speechVolume; set { speechVolume = value; OnSettingChange?.Invoke(); } }
    public int Language { get => language; set { language = value; OnSettingChange?.Invoke(); } }
}
