using UnityEngine;
using UnityEngine.UI;

public class MenuPanel : MonoBehaviour
{
    public Button StartTutorialButton;
    public Button StartGameTableButton;
    public Button StartGamePedestalsButton;
    public Button StartGameWalkingButton;
    public Button StartFloodButton;
    public Button CloseGameButton;
    public Slider LengthSettingSlider;
    public Slider FloodSettingSlider;

    public SceneTransitionScript SceneTransitionScript;

    private float ButtonTimeout = 0;

    private void Update()
    {
        if(ButtonTimeout > 0)
        {
            ButtonTimeout -= Time.deltaTime;
        }
    }

    private void Awake()
    {
        //StartTutorialButton.onClick.AddListener(StartTutorial);
        StartGameTableButton.onClick.AddListener(StartGameTable);
        StartGamePedestalsButton.onClick.AddListener(StartGamePedestals);
        StartGameWalkingButton.onClick.AddListener(StartGameWalking);
        //LengthSettingSlider.onValueChanged.AddListener(LengthSettingSliderMoved);
        //StartFloodButton.onClick.AddListener(StartFlood);
        //FloodSettingSlider.onValueChanged.AddListener(FloodSettingSliderMoved);
        CloseGameButton.onClick.AddListener(QuitGame);
    }

    private void Start()
    {
        LengthSettingSlider.value = StartArguments.Instance.TimeInMinutes;
        FloodSettingSlider.value = StartArguments.Instance.MaxPackages;
    }

    private void OnDestroy()
    {
        StartTutorialButton.onClick.RemoveAllListeners();
        StartGameTableButton.onClick.RemoveAllListeners();
    }

    private void QuitGame() {
        Application.Quit();
    }

    private void StartFlood() {
        StartArguments.Instance.lenghtMode = StartArguments.LenghtMode.OVERFLOW;
        SceneTransitionScript.OpenRunScene();
    }

    private void StartTutorial() {
        SceneTransitionScript.OpenTutorialScene();
    }

    private void StartGameTable() {
        if(ButtonTimeout <= 0)
        {
            LengthSettingSliderMoved(LengthSettingSlider.value);
            SceneTransitionScript.OpenTableScene();
            ButtonTimeout = 1f;
        }
    }

    private void StartGamePedestals() {
        if (ButtonTimeout <= 0)
        {
            LengthSettingSliderMoved(LengthSettingSlider.value);
            SceneTransitionScript.OpenPedestalsScene();
            ButtonTimeout = 1f;
        }
    }

    private void StartGameWalking() {
        if (ButtonTimeout <= 0)
        {
            LengthSettingSliderMoved(LengthSettingSlider.value);
            SceneTransitionScript.OpenWalkingScene();
            ButtonTimeout = 1f;
        }
    }

    private void LengthSettingSliderMoved(float f) {
        if (f == LengthSettingSlider.maxValue)
        {
            StartArguments.Instance.lenghtMode = StartArguments.LenghtMode.UNLIMITED;
        }
        else if (f == LengthSettingSlider.minValue)
        {
            StartArguments.Instance.lenghtMode = StartArguments.LenghtMode.PACKAGE_COUNT;
            StartArguments.Instance.PackageCount = 5;
        }
        else
        {
            StartArguments.Instance.lenghtMode = StartArguments.LenghtMode.TIME;
        }
        StartArguments.Instance.TimeInMinutes = f;
    }

    private void FloodSettingSliderMoved(float f) {
        StartArguments.Instance.MaxPackages = (int)f;
    }
}
