using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MultipleButtonsSetting : MonoBehaviour
{
    [SerializeField]
    private Button[] ButtonOptions;
    [SerializeField]
    private Sprite ButtonPressedSprite;
    [SerializeField]
    private Color ButtonPressedColor;
    [SerializeField]
    private Sprite ButtonDefaultSprite;
    [SerializeField]
    private Color ButtonDefaultColor;
    public int CurrentValue = -1; 
    public UnityAction<int> OnValueChanged;

    private void Awake()
    {
        ButtonClicked(0);
    }

    public void ButtonClicked(int index) {
        if (CurrentValue == index) return;
        CurrentValue = index;
        for (int i = 0; i < ButtonOptions.Length; i++)
        {
            if (i == CurrentValue)
            {
                ButtonOptions[i].image.sprite = ButtonPressedSprite;
                ButtonOptions[i].image.color = ButtonPressedColor;
            }
            else {
                ButtonOptions[i].image.sprite = ButtonDefaultSprite;
                ButtonOptions[i].image.color = ButtonDefaultColor;
            }
        }
        OnValueChanged?.Invoke(CurrentValue);
    }
}
