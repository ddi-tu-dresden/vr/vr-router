﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransitionScript : MonoBehaviour
{
    public Animator Animator;
    private bool sceneTransitioning = false;

    private void Awake()
    {
        sceneTransitioning = false;
    }

    private void LoadSceneIfPossible(int i)
    {
        if (sceneTransitioning) return;
        else
        {
            sceneTransitioning = true;
            StartCoroutine(LoadScene(i));
        }
    }

    public void OpenRunScene()
    {
        int sceneID = 1;
        if(StartArguments.Instance.version == StartArguments.Version.WALKING)
        {
            sceneID = 2;
        }

        LoadSceneIfPossible(sceneID);
    }

    public void OpenTableScene()
    {
        StartArguments.Instance.mode = StartArguments.StartMode.RANDOM;
        LoadSceneIfPossible(1);
    }

    public void OpenPedestalsScene()
    {
        StartArguments.Instance.mode = StartArguments.StartMode.RANDOM;
        LoadSceneIfPossible(2);
    }

    public void OpenWalkingScene()
    {
        StartArguments.Instance.mode = StartArguments.StartMode.RANDOM;
        LoadSceneIfPossible(3);
    }

    public void OpenTutorialScene()
    {
        StartArguments.Instance.mode = StartArguments.StartMode.TUTORIAL;
        LoadSceneIfPossible(1);
    }

    IEnumerator LoadScene(int v)
    {
        Debug.Log("Loading scene " + v);
        Animator.SetTrigger("StartTransition");

        yield return new WaitForEndOfFrame();
        //yield return new WaitForSeconds(3);

        var asyncLoad = SceneManager.LoadSceneAsync(v, LoadSceneMode.Single);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
