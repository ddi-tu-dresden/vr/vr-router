﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using BezierSolution;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine.InputSystem;
using Animui;

public class RunTutorialController : RunController
{
    public TutorialManager TutorialManager;
    public TutorialAgent TutorialAgent;
    public TMP_Text InstructionText;

    private PlopUpGraphical InstructionTextPlopUp;

    private void OnEnable()
    {
        Settings.Instance.OnSettingChange += OnSettingsChange;
    }

    private void OnDisable()
    {
        Settings.Instance.OnSettingChange -= OnSettingsChange;
    }

    void OnSettingsChange() {
        InstructionText.alpha = Settings.Instance.InstructionVisibility == Settings.TextMode.HIDDEN ? 0f : 1f;
    }

    private void Awake()
    {
        InstructionTextPlopUp = InstructionText.GetComponent<PlopUpGraphical>();
    }

    private void Start()
    {
        if (StartArguments.Instance.mode == StartArguments.StartMode.TUTORIAL)
        {
            StartCoroutine(RunTutorialRoutine());
        }
        else {
            Destroy(TutorialAgent.gameObject);
            Destroy(gameObject);
        }
        OnSettingsChange();
    }

    bool packageCorrectlyDelivered = false;
    IEnumerator RunTutorialRoutine() {
        //counting the packages we need
        foreach (TutorialStep step in TutorialManager.steps)
        {
            if (step.Class == TutorialStepClass.SpawnPackage || step.Class == TutorialStepClass.SpawnAndDeliverPackage)
            {
                packageCount++;
            }
        }
        //before generating the run
        if (packageCount > 0) InitializeRun();

        //running the tutorial
        for (int i = 0; i < TutorialManager.steps.Length; i++)
        {
            float stepStartTime = Time.time;
            TutorialAgent.DesiredLocation = TutorialManager.steps[i].transform.position;
            SetTextAndSoundForTutorialStep(TutorialManager.steps[i].Text, TutorialManager.steps[i].Sound);
            switch (TutorialManager.steps[i].Class)
            {
                case TutorialStepClass.Timed:
                    yield return new WaitForSeconds(TutorialManager.steps[i].TimeInSeconds);
                    break;
                case TutorialStepClass.ButtonPress:
                    foreach (InputActionReference actionRef in TutorialManager.steps[i].ListenToAction)
                    {
                        actionRef.action.Enable();
                    }
                    while (true)
                    {
                        if (SecondaryInstructionNeeded(i, stepStartTime))
                        {
                            stepStartTime = Time.time;
                            SetTextAndSoundForTutorialStep(TutorialManager.steps[i].SecondaryText, TutorialManager.steps[i].SecondarySound);
                        }
                        foreach (InputActionReference actionRef in TutorialManager.steps[i].ListenToAction)
                        {
                            if (actionRef.action.IsPressed()) goto endOfLoop;
                        }
                        yield return null;
                    }
                    endOfLoop:
                    yield return new WaitForSeconds(Mathf.Max(0f, TutorialManager.steps[i].TimeInSeconds - (Time.time - stepStartTime)));
                    break;
                case TutorialStepClass.SpawnPackage:
                    packageCorrectlyDelivered = false;
                    Run.CorrectPackagePlacement += PackageCorrectlyDelivered;
                    SpawnNextPackage((int)TutorialManager.steps[i].SpawnPackageConfiguration);
                    yield return new WaitForSeconds(TutorialManager.steps[i].TimeInSeconds);
                    break;
                case TutorialStepClass.DeliverPackage:
                    while (!packageCorrectlyDelivered)
                    {
                        if (SecondaryInstructionNeeded(i, stepStartTime))
                        {
                            stepStartTime = Time.time;
                            SetTextAndSoundForTutorialStep(TutorialManager.steps[i].SecondaryText, TutorialManager.steps[i].SecondarySound);
                        }
                        yield return null;
                    }
                    Run.CorrectPackagePlacement -= PackageCorrectlyDelivered;
                    yield return new WaitForSeconds(Mathf.Max(0f, TutorialManager.steps[i].TimeInSeconds - (Time.time - stepStartTime)));
                    break;
                case TutorialStepClass.SpawnAndDeliverPackage:
                    packageCorrectlyDelivered = false;
                    Run.CorrectPackagePlacement += PackageCorrectlyDelivered;
                    SpawnNextPackage((int)TutorialManager.steps[i].SpawnPackageConfiguration);
                    while (!packageCorrectlyDelivered)
                    {
                        if (SecondaryInstructionNeeded(i, stepStartTime))
                        {
                            stepStartTime = Time.time;
                            SetTextAndSoundForTutorialStep(TutorialManager.steps[i].SecondaryText, TutorialManager.steps[i].SecondarySound);
                        }
                        yield return null;
                    }
                    Run.CorrectPackagePlacement -= PackageCorrectlyDelivered;
                    yield return new WaitForSeconds(Mathf.Max(0f, TutorialManager.steps[i].TimeInSeconds - (Time.time - stepStartTime)));
                    break;
                case TutorialStepClass.SendMessageToSceneController:
                    SendMessage(TutorialManager.steps[i].Text);
                    yield return new WaitForSeconds(TutorialManager.steps[i].TimeInSeconds);
                    break;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    private bool SecondaryInstructionNeeded(int i, float stepStartTime)
    {
        return TutorialManager.steps[i].ProvideSecondaryInstructions && Time.time > stepStartTime + TutorialManager.steps[i].SecondaryDelay;
    }

    private void SetTextAndSoundForTutorialStep(string Text, AudioClip Sound)
    {
        if (Sound != null)
        {
            TutorialAgent.Source.clip = Sound;
            TutorialAgent.Source.Play();
        }
        InstructionText.text = Text;
        if (Text != "")
        {
            InstructionTextPlopUp.PlayAnimation(0);
        }
    }

    private void PackageCorrectlyDelivered(object sender, RunEventArgs e) {
        packageCorrectlyDelivered = true;
    }

    protected override void SubscribeToTubeEvents()
    {
        Run.WrongPackagePlacement += (obj, args) => BouncePackage(obj, (Package)args.Package);
    }
}
