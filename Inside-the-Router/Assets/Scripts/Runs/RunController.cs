using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OmiLAXR.xAPI.Tracking;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RunController : MonoBehaviour
{
    public static RunController CurrentRunController;

    public List<GameObject> UiTubes = new();
    public List<PackageScript> CurrentPackages = new();
    public GameObject PackagePrefab;
    public float StartTime { protected set; get; }

    protected int tubeCount = 5;
    protected int packageCount = 5;

    private int packageCounter = 1;

    public virtual void InitializeRun()
    {
        CurrentRunController = this;
        IpUtils.ResetIpCounter();
        Run run = new(tubeCount);

        //Match Tubes between Front- and Backend
        if (UiTubes.Count != tubeCount)
            throw new System.Exception("Number of Tubes differes between Front- and Backend");
        for (int i = 0; i < tubeCount; i++)
        {
            UiTubes[i].GetComponent<TubeScript>().SetTube(run.Tubes[i]);
        }

        SubscribeToTubeEvents();

        //Lets go!
        run.Start();
        StartTime = Time.time;
    }

    protected virtual void SubscribeToTubeEvents()
    {
        //Get notified if Player is ready for the next package

        Run.CorrectPackagePlacement += OnCorrectPackagePlacement;
        Run.WrongPackagePlacement += OnWrongPackagePlacement;
    }

    protected virtual void UnsubsribeFromTubeEvents()
    {
        Run.CorrectPackagePlacement -= OnCorrectPackagePlacement;
        Run.WrongPackagePlacement -= OnWrongPackagePlacement;
    }

    private void OnWrongPackagePlacement(object sender, RunEventArgs e)
    {
        BouncePackage(sender, (Package)e.Package);
    }

    private void OnCorrectPackagePlacement(object sender, RunEventArgs e)
    {
        SpawnNextPackage();
    }

    protected void BouncePackage(object obj, Package pckg)
    {
        var wrongTube = obj as Tube;
        SpawnPackageFromTube(pckg, wrongTube);
    }

    public void DestroyPackage(PackageScript pckg)
    {
        CurrentPackages.Remove(pckg);
        Destroy(pckg.gameObject);
    }

    protected virtual void SpawnNextPackage(int difficulty = 0)
    {
        //fetch Package
        var package = Run.CurrentRun.GenerateNextPackage(difficulty);

        if (package != null)
        {
            //Select Source Tube
            Tube sourceTube = null;

            if (Run.CurrentRun.Tubes.Any(t => t.IpAddress == package.Source.IpAddress))
            {
                //Select Tube with matching IpAddress
                sourceTube = Run.CurrentRun.Tubes.First(t => t.IpAddress == package.Source.IpAddress);
            }
            else
            {
                //Select WAN Tube
                sourceTube = Run.CurrentRun.Tubes.Last();
            }

            SpawnPackageFromTube(package, sourceTube);
            FlashTubeForAttention(sourceTube);
        }
        else
        {
            EndRun();
        }
    }

    internal void FlashTubeForAttention(Tube sourceTube)
    {
        var uiTube = UiTubes[Run.CurrentRun.Tubes.IndexOf(sourceTube)];
        uiTube.GetComponent<TubeScript>().FlashForSeconds(3.5f);
    }

    protected void SpawnPackageFromTube(Package package, Tube tube)
    {
        var i = Run.CurrentRun.Tubes.IndexOf(tube);
        var uiTube = UiTubes[i];


        //Instantiate and Throw Package
        var uiPackage = Instantiate(PackagePrefab, uiTube.transform.position, Quaternion.identity);
        PackageScript pckgC = uiPackage.GetComponent<PackageScript>();

        //Mute Collider of Selected Tube
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == "StudyWalking")
        {
            uiTube.GetComponent<TubeScript>().MuteCollider(pckgC.GetComponent<Collider>(), 10f);
        }
        else
        {
            uiTube.GetComponent<TubeScript>().MuteCollider(pckgC.GetComponent<Collider>(), 1f);
        }

        pckgC.SetDataPackage(package);
        pckgC.StartFromTube(uiTube);
        CurrentPackages.Add(pckgC);
    }

    public virtual void EndRun(float delay = 5f)
    {
        UnsubsribeFromTubeEvents();
        // Switch to Menu after some seconds
        StartCoroutine(SwitchToMenuScene(delay));

        foreach (PackageScript pckg in CurrentPackages)
        {
            Destroy(pckg.gameObject);
        }

        CurrentPackages.Clear();
        Run.CurrentRun?.End();        
    }

    private IEnumerator SwitchToMenuScene(float delay = 5f)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadSceneAsync(0, LoadSceneMode.Single);
    }
}