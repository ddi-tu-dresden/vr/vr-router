﻿
public interface ILocalizable
{
    void SyncLocalization(string value);

    string GetKey();

    string DebugErrorPrintName();
}
