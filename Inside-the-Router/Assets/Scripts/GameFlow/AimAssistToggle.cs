using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimAssistToggle : MonoBehaviour
{
    private void Start()
    {
        Settings.Instance.OnSettingChange += ApplyAimAssist;
        ApplyAimAssist();
    }

    private void OnDestroy()
    {
        Settings.Instance.OnSettingChange -= ApplyAimAssist;
    }

    private void ApplyAimAssist()
    {
        gameObject.SetActive(Settings.Instance.AimAssist == Settings.AimAssistMode.ON);
    }
}
