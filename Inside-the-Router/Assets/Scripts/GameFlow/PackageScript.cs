﻿using System;
using System.Collections;
using BezierSolution;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;

/// <summary>
/// This script is assigned to a Package Prefab and handles the behaviour of the package during its lifecycle as well as after different player actions.
/// </summary>
public class PackageScript : FeedbackChannel
{
    public TMP_Text PackageLabel = null;
    public AudioSource OutSound;
    public AudioSource SpawnSound;
    private Rigidbody packageRigidbody;
    private XRVelocityInteractor velocityInteractor;
    private XRVelocityInteractor ownVelocityInteractor;
    private bool kinematicPrecisionThrow = false;
    public RespawnAnimationScript respawnAnimationScript;
    public GameObject RespawnPoint;


    public event EventHandler<RunEventArgs> PackageGripped;
    public event EventHandler<RunEventArgs> PackageThrown;
    public event EventHandler<RunEventArgs> PackageRespawn;

    private Package dataPackage = null;
    private Vector3 tempThrowVector;
    private Vector3 throwRotation;
    private float tempThrowSpeed;
    private Vector3 gravity = new(0f, -0.81f, 0f);

    private readonly float meanThrowSpeed = 14f;
    private readonly float throwSpeedChangeWithForce = 4f;
    private readonly float rotationDamping = 0.95f;
    private UnityAction languageUpdateAction;

    private Color baseColor = Color.yellow;
    private bool isFeedbackActive = false;

    private void Awake()
    {
        packageRigidbody = GetComponent<Rigidbody>();
        ownVelocityInteractor = GetComponent<XRVelocityInteractor>();
        gravity *= meanThrowSpeed;
        languageUpdateAction = delegate { SetDataPackage(dataPackage); };

        if(RespawnPoint == null)
        {
            RespawnPoint = GameObject.Find("RespawnPoint");
        }
    }

    protected override void Start()
    {
        SetRandomColor();
        baseColor = PackageLabel.color;
        Localization.Get.OnLanguageChanged.AddListener(languageUpdateAction);


        base.Start();
    }


    protected override void OnDestroy()
    {
        base.OnDestroy();
        Localization.Get.OnLanguageChanged.RemoveListener(languageUpdateAction);
    }

    private void SetRandomColor()
    {
        Renderer rend = GetComponent<Renderer>();
        Material[] materialSet = rend.materials;
        materialSet[0].color = new Color(UnityEngine.Random.Range(0f, 1f) * 0.8f,
            UnityEngine.Random.Range(0f, 1f) * 0.8f, UnityEngine.Random.Range(0f, 1f) * 0.8f);
        rend.materials = materialSet;
    }

    private void FixedUpdate()
    {
        if (kinematicPrecisionThrow)
        {
            //apply the current velocity
            transform.position += tempThrowVector * Time.fixedDeltaTime;

            //change the throw vector over time with gravity
            tempThrowVector += gravity * Time.fixedDeltaTime;

            //retains some initial rotation force from the throw
            transform.eulerAngles += throwRotation * Time.fixedDeltaTime;

            //which decays over time. note that rotationDamping does not need to be multiplied with fixed delta time, as fixed delta time is constant
            throwRotation *= rotationDamping;

            //gradually rotates towards its target
            transform.rotation = Quaternion.RotateTowards(transform.rotation,
                Quaternion.LookRotation(ownVelocityInteractor.velocity, Vector3.forward),
                120f * tempThrowSpeed * Time.fixedDeltaTime);
        }
    }

    private void Update()
    {
        if (transform.position.x > WallScript.XHigh || transform.position.x < WallScript.XLow ||
            transform.position.y > WallScript.YHigh || transform.position.y < WallScript.YLow ||
            transform.position.z > WallScript.ZHigh || transform.position.z < WallScript.ZLow)
        {
            Respawn();
        }
    }

    /// <summary>
    /// formats the package label corresponding the the passed meta object
    /// </summary>
    public void SetDataPackage(Package p)
    {
        dataPackage = p;

        if (PackageLabel == null || p == null) return;

        PackageLabel.text =
            $"{Localization.Get.Phrase("package_source")}: {IpUtils.ConvertAddressToString(p.Source)}\n{Localization.Get.Phrase("package_destination")}: {IpUtils.ConvertAddressToString(p.Destination)}";
    }

    public Package GetDataPackage()
    {
        return dataPackage;
    }

    /// <summary>
    /// commences an animation of the package from a tube to the table in front of the player
    /// </summary>
    public void StartFromTube(GameObject uiTube)
    {
        packageRigidbody.isKinematic = true;
        kinematicPrecisionThrow = false;
        var walker = GetComponent<BezierWalkerWithSpeed>();
        // walker.speed = 6f;
        walker.spline = uiTube.transform.Find("Spline").GetComponent<BezierSpline>();
        walker.enabled = true;
        OutSound.Play();
    }

    public void DisapperInTube(GameObject uiTube)
    {
        packageRigidbody.isKinematic = true;
        kinematicPrecisionThrow = false;
        var walker = GetComponent<BezierWalkerWithSpeed>();
        // walker.speed = 6f;
        
        walker.NormalizedT = 0f;
        
        walker.spline = uiTube.transform.Find("SplineReverse").GetComponent<BezierSpline>();
        walker.enabled = true;
    }

    /// <summary>
    /// cancels the animation and respawns the package
    /// </summary>
    public void DeactivateWalker()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == "StudyWalking")
        {
            packageRigidbody.useGravity = false;
        }
        else
        {
            packageRigidbody.isKinematic = false;
            packageRigidbody.useGravity = true;
        }

        kinematicPrecisionThrow = false;
        GetComponent<BezierWalkerWithSpeed>().enabled = false;
        if (packageRigidbody != null)
        {
            packageRigidbody.velocity = new Vector3(0, 0, 0);
            packageRigidbody.position = new Vector3(8, 1.75f, -0.25f);
        }
    }

    /// <summary>
    /// respawns the package at the table in front of the player (sets position and physics, plays the sound and plays the respawn animation)
    /// </summary>
    public void Respawn()
    {
        packageRigidbody.isKinematic = true;
        kinematicPrecisionThrow = false;
        SpawnSound.Play();

        PackageRespawn?.Invoke(this, new RunEventArgs("Package Respawn", dataPackage));

        if (packageRigidbody != null)
        {
            packageRigidbody.velocity = Vector3.zero;
            packageRigidbody.angularVelocity = Vector3.zero;
            packageRigidbody.position = RespawnPoint.transform.position;
        }

        respawnAnimationScript.PlayAnimation();
    }

    public void onGripStarted(SelectEnterEventArgs selectEnterEventArgs)
    {
        velocityInteractor = selectEnterEventArgs.interactorObject.transform.GetComponent<XRVelocityInteractor>();
        if (velocityInteractor == null)
            Debug.LogError("No XRVelocityInteractor found on Controller objects. Please add one.");
        velocityInteractor.enabled = true;
        velocityInteractor.Reset();
        packageRigidbody.isKinematic = true;
        kinematicPrecisionThrow = false;
        PackageGripped?.Invoke(this, new RunEventArgs("Package Gripped", dataPackage));
    }

    public void onReleaseGrip()
    {
        packageRigidbody.useGravity = true;

        //disabeling the velocity interactor on the controller that moved the package saves performance as these informations are not needed;
        //the velocity interactor on the controller is reset on the next grip anyway
        velocityInteractor.enabled = false;

        AttemptDirectThrowToPipe();
    }

    /// <summary>
    /// starts a throw if the velocity is high enough. Threshhold is lower if directed at valid target.
    /// if below threshhold, drops the package
    /// </summary>
    private void AttemptDirectThrowToPipe()
    {
        RaycastHit raycastHit;
        throwRotation = velocityInteractor.rotation;
        packageRigidbody.isKinematic = true;
        kinematicPrecisionThrow = true;

        //calculates the tempThrowSpeed which is higher if you throw with more force
        tempThrowSpeed = meanThrowSpeed + (Mathf.Clamp01((velocityInteractor.velocity.magnitude - 1f) / 2f) * 2 - 1f) *
            throwSpeedChangeWithForce;

        // if game object with layer 12 (ThrowHelper) will be hit
        if (Physics.Raycast(transform.position, velocityInteractor.velocity, out raycastHit, Mathf.Infinity,
                1 << 12)) // LayerMask 12 -> ThrowHelper Layer
        {
            // HIT --- aim assist on
            if (velocityInteractor.velocity.magnitude < 0.1f)
            {
                packageRigidbody.isKinematic = false;
                kinematicPrecisionThrow = false;
                return;
            }

            float closestDistance = Mathf.Infinity;
            int targetChampionIndex = -1;
            float distance;
            for (int i = 0; i < PossibleThrowTargets.targets.list.Count; i++)
            {
                distance = Vector3.Distance(PossibleThrowTargets.targets.list[i].position, raycastHit.point);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    targetChampionIndex = i;
                }
            }

            if (targetChampionIndex == -1)
            {
                Debug.Log("Invalid throwing result. Something went wrong.");
            }
            else
            {
                tempThrowVector =
                    (PossibleThrowTargets.targets.list[targetChampionIndex].position +
                        (-0.18f * Mathf.Pow(meanThrowSpeed / tempThrowSpeed, 3.0f) * gravity) - transform.position)
                    .normalized * tempThrowSpeed;
            }
        }
        else
        {
            // no hit with layer 12 (ThrowHelper) --- aim assist off
            if (velocityInteractor.velocity.magnitude < 1.2f) // magnitude = length of vector
            {
                packageRigidbody.isKinematic = false;
                kinematicPrecisionThrow = false;
                return;
            }

            tempThrowVector = velocityInteractor.velocity.normalized * tempThrowSpeed;
        }

        PackageThrown?.Invoke(this, new RunEventArgs("Package Thrown", dataPackage));
    }

    [ContextMenu("BlinkTest")]
    public void TestFeedback()
    {
        OnFeedbackRequested(FeedbackCase.UNKNOWN);
    }


    public override void OnFeedbackRequested(FeedbackCase fbc)
    {
        StartCoroutine(HighlightDestination());
    }

    public IEnumerator HighlightDestination()
    {
        Debug.Log("Enum_Fade activated");
        isFeedbackActive = true;

        string[] originalStrings = PackageLabel.text.Split("\n");


        while (isFeedbackActive)
        {
            PackageLabel.text =
                originalStrings[0] + "\n" +
                "<color=\"green\">" +
                originalStrings[1] +
                "</color>";
            yield return new WaitForSeconds(1);
            PackageLabel.text =
                originalStrings[0] + "\n" +
                "<color=#FF8A00>" +
                originalStrings[1] +
                "</color>";
            yield return new WaitForSeconds(1);
        }

        yield return null;
    }
}