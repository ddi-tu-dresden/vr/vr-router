using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteLookAtTransform : MonoBehaviour
{
    public Transform tf;

    private void Update()
    {
        transform.LookAt(tf);
    }
}
