﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script handles the respawn animation of the package
/// </summary>
public class RespawnAnimationScript : MonoBehaviour
{
    public GameObject[] children;

    /// <summary>
    /// Plays the respawn animation
    /// </summary>
    public void PlayAnimation()
    {
        foreach (GameObject obj in children)
        {
            obj.SetActive(true);
        }
        GetComponent<Animator>().SetTrigger("PlayRespawnAnimation");
        if (!deactivateGameObjectsRoutineRunning)
        {
            StartCoroutine(DeactivateGameObjects());
        }
    }

    /// <summary>
    /// deactivates the animation objects after 3 seconds. Can only run once at a time.
    /// </summary>
    bool deactivateGameObjectsRoutineRunning = false;
    private IEnumerator DeactivateGameObjects()
    {
        deactivateGameObjectsRoutineRunning = true;
        yield return new WaitForSeconds(3);
        DeactivateAllChildren();
        deactivateGameObjectsRoutineRunning = false;
    }

    /// <summary>
    /// Deactivates all children game objects. Also called from interactable component when grabbing the package.
    /// </summary>
    public void DeactivateAllChildren()
    {
        foreach (GameObject obj in children)
        {
            obj.SetActive(false);
        }
    }
}
