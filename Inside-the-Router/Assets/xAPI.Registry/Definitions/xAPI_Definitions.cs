
namespace xAPI.Registry {
    /// <summary>
    /// Static class that provides all contexts as public properties.
    /// </summary>
    public static class xAPI_Definitions {
        /// <summary>
        /// 
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/eyeTracking
        /// </summary>
        public static readonly xAPI_Context_EyeTracking eyeTracking = new xAPI_Context_EyeTracking();

        /// <summary>
        /// 
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/generic
        /// </summary>
        public static readonly xAPI_Context_Generic generic = new xAPI_Context_Generic();

        /// <summary>
        /// 
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/gestures
        /// </summary>
        public static readonly xAPI_Context_Gestures gestures = new xAPI_Context_Gestures();

        /// <summary>
        /// 
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms
        /// </summary>
        public static readonly xAPI_Context_Lms lms = new xAPI_Context_Lms();

        /// <summary>
        /// 
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/media
        /// </summary>
        public static readonly xAPI_Context_Media media = new xAPI_Context_Media();

        /// <summary>
        /// 
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/multitouch
        /// </summary>
        public static readonly xAPI_Context_Multitouch multitouch = new xAPI_Context_Multitouch();

        /// <summary>
        /// 
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/observation
        /// </summary>
        public static readonly xAPI_Context_Observation observation = new xAPI_Context_Observation();

        /// <summary>
        /// 
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/projectJupyter
        /// </summary>
        public static readonly xAPI_Context_ProjectJupyter projectJupyter = new xAPI_Context_ProjectJupyter();

        /// <summary>
        /// 
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/seriousGames
        /// </summary>
        public static readonly xAPI_Context_SeriousGames seriousGames = new xAPI_Context_SeriousGames();

        /// <summary>
        /// 
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/systemControl
        /// </summary>
        public static readonly xAPI_Context_SystemControl systemControl = new xAPI_Context_SystemControl();

        /// <summary>
        /// 
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/tagformance
        /// </summary>
        public static readonly xAPI_Context_Tagformance tagformance = new xAPI_Context_Tagformance();

        /// <summary>
        /// 
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/uhfReader
        /// </summary>
        public static readonly xAPI_Context_UhfReader uhfReader = new xAPI_Context_UhfReader();

        /// <summary>
        /// 
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/virtualReality
        /// </summary>
        public static readonly xAPI_Context_VirtualReality virtualReality = new xAPI_Context_VirtualReality();

        /// <summary>
        /// 
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/voice
        /// </summary>
        public static readonly xAPI_Context_Voice voice = new xAPI_Context_Voice();

        /// <summary>
        /// 
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/vrRfidChamber
        /// </summary>
        public static readonly xAPI_Context_VrRfidChamber vrRfidChamber = new xAPI_Context_VrRfidChamber();
    }
}