﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class AddressTest
    {
        // A Test behaves as an ordinary method
        [Test]
        public void EqualsTest()
        {
            var ip1 = IpUtils.GetUInt32FromIPAddress("192.168.0.1");
            var ip2 = IpUtils.GetUInt32FromIPAddress("192.168.1.1");

            Address a1 = new Address(ip1, 21);
            Address b1 = new Address(ip2, 21);
            Address c1 = new Address(ip1, 22);
            Address d1 = new Address(ip2, 22);

            Address a2 = new Address(ip1, 21);
            Address b2 = new Address(ip2, 21);
            Address c2 = new Address(ip1, 22);
            Address d2 = new Address(ip2, 22);

            //TODO: eigene Equals verwenden
            //Optional: auf AreNotSame testen
            Assert.IsTrue(a1.Equals(a2));
            Assert.IsTrue(b1.Equals(b2));
            Assert.IsTrue(c1.Equals(c2));
            Assert.IsTrue(d1.Equals(d2));

            Assert.AreNotSame(a1, a2);
            Assert.AreNotSame(b1, b2);
            Assert.AreNotSame(c1, c2);
            Assert.AreNotSame(d1, d2);

            Assert.IsFalse(a1.Equals(b1));
            Assert.IsFalse(a1.Equals(c1));
            Assert.IsFalse(a1.Equals(d1));
        }
    }
}
