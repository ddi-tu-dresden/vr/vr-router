﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{

    public class TubeTest
    {
        Tube in1 = new Tube() { IpAddress = IpUtils.GetUInt32FromIPAddress("192.168.1.100") };
        Tube in2 = new Tube() { IpAddress = IpUtils.GetUInt32FromIPAddress("192.168.1.101") };
        Tube exTube = new Tube() { IpAddress = IpUtils.ExternalRouterIP };
        UInt32 extIp = IpUtils.GetUInt32FromIPAddress("72.167.34.1");
        UInt32 server = IpUtils.GetUInt32FromIPAddress("123.1.2.3");

        private void prepareTables()
        {

            var nat1 = new NatTableEntry()
            {
                ExternalAddress = new Address(extIp, 6001),
                InternalAddress = new Address(in1.IpAddress, 51)
            };

            var nat2 = new NatTableEntry()
            {
                ExternalAddress = new Address(extIp, 6002),
                InternalAddress = new Address(in2.IpAddress, 52)
            };

            var nat3 = new NatTableEntry()
            {
                ExternalAddress = new Address(extIp, 6003),
                InternalAddress = new Address(in2.IpAddress, 53)
            };

            NatTable.Entries.Clear();
            NatTable.Entries.Add(nat1);
            NatTable.Entries.Add(nat2);
            NatTable.Entries.Add(nat3);
        }

        [Test]
        public void HandleCorrectPackageTest()
        {
            /*
             * Prepare
             */
            prepareTables();


            var package_1 = new Package(new Address(in1.IpAddress, 51), new Address(server, 51));
            var package_2 = new Package(new Address(server, 52), new Address(extIp, 6002));
            var package_3 = new Package(new Address(in2.IpAddress, 53), new Address(server, 53));
            var package_4 = new Package(new Address(server, 53), new Address(extIp, 6003));


            //Flags
            List<String> TubeEventLog = new List<String>();
            in1.CorrectPackageReceived += (s, e) => TubeEventLog.Add("Tube 1: " + e.Type);
            in2.CorrectPackageReceived += (s, e) => TubeEventLog.Add("Tube 2: " + e.Type);
            exTube.CorrectPackageReceived += (s, e) => TubeEventLog.Add("Tube E: " + e.Type);

            /*
             * Act
             */
            exTube.HandlePackage(package_1);
            in2.HandlePackage(package_2);
            exTube.HandlePackage(package_3);
            in2.HandlePackage(package_4);


            /*
             * Check
             */
            Assert.AreEqual(4, TubeEventLog.Count);
            Assert.AreEqual("Tube E: CorrectPackageReceived", TubeEventLog[0]);
            Assert.AreEqual("Tube 2: CorrectPackageReceived", TubeEventLog[1]);
            Assert.AreEqual("Tube E: CorrectPackageReceived", TubeEventLog[2]);
            Assert.AreEqual("Tube 2: CorrectPackageReceived", TubeEventLog[3]);
        }

        [Test]
        public void HandleWrongPackageTest()
        {
            /*
             * Prepare
             */
            prepareTables();


            var package_1 = new Package(new Address(in1.IpAddress, 51), new Address(server, 51));
            var package_2 = new Package(new Address(server, 52), new Address(extIp, 6002));
            var package_3 = new Package(new Address(in2.IpAddress, 53), new Address(server, 53));
            var package_4 = new Package(new Address(server, 53), new Address(extIp, 6003));


            //Flags
            List<String> TubeEventLog = new List<String>();
            in1.WrongPackageReceived += (s, e) => TubeEventLog.Add("Tube 1: " + e.Type);
            in2.WrongPackageReceived += (s, e) => TubeEventLog.Add("Tube 2: " + e.Type);
            exTube.WrongPackageReceived += (s, e) => TubeEventLog.Add("Tube E: " + e.Type);

            /*
             * Act
             */
            in1.HandlePackage(package_1);
            in1.HandlePackage(package_2);
            in1.HandlePackage(package_3);
            exTube.HandlePackage(package_4);


            /*
             * Check
             */
            Assert.AreEqual(4, TubeEventLog.Count);
            Assert.AreEqual("Tube 1: WrongPackageReceived", TubeEventLog[0]);
            Assert.AreEqual("Tube 1: WrongPackageReceived", TubeEventLog[1]);
            Assert.AreEqual("Tube 1: WrongPackageReceived", TubeEventLog[2]);
            Assert.AreEqual("Tube E: WrongPackageReceived", TubeEventLog[3]);
        }

        [Test]
        public void CreateTubesTest()
        {
            var tubes = Tube.CreateTubes(5);

            Assert.AreEqual(5, tubes.Count);
            Assert.IsTrue(IpUtils.ConvertUIntToString(tubes[0].IpAddress).Contains("192.168"));
            Assert.IsTrue(IpUtils.ConvertUIntToString(tubes[1].IpAddress).Contains("192.168"));
            Assert.IsTrue(IpUtils.ConvertUIntToString(tubes[2].IpAddress).Contains("192.168"));
            Assert.IsTrue(IpUtils.ConvertUIntToString(tubes[3].IpAddress).Contains("192.168"));
            Assert.IsTrue(IpUtils.ConvertUIntToString(tubes[4].IpAddress).Contains("192.168"));

        }
    }
}