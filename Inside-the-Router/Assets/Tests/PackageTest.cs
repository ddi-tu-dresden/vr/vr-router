﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class PackageTest
    {
        [Test]
        public void generateInternToExternPackageTest()
        {
            var packages = new List<Package>();

            for (int i = 0; i < 100; i++)
            {
                packages.Add(new Package(true, false));
            }

            Assert.IsTrue(packages.TrueForAll(p => p.Source.IpAddress != 0), "Source not valid/initialized");
            Assert.IsTrue(packages.TrueForAll(p => p.Source.Port != 0), "Source not valid/initialized");
            Assert.IsTrue(packages.TrueForAll(p => p.Destination.IpAddress != 0), "Destination not valid/initialized");
            Assert.IsTrue(packages.TrueForAll(p => p.Destination.Port != 0), "Destination not valid/initialized");
            Assert.IsTrue(packages.TrueForAll(p => p.Destination.IpAddress != p.Source.IpAddress), "Source cannot be Destination Address");
        }

        [Test]
        public void generateExternToInternPackageTest()
        {
            var packages = new List<Package>();

            for (int i = 0; i < 100; i++)
            {
                packages.Add(new Package(false, true));
            }

            Assert.IsTrue(packages.TrueForAll(p => p.Source.IpAddress != 0), "Source not valid/initialized");
            Assert.IsTrue(packages.TrueForAll(p => p.Source.Port != 0), "Source not valid/initialized");
            Assert.IsTrue(packages.TrueForAll(p => p.Destination.IpAddress != 0), "Destination not valid/initialized");
            Assert.IsTrue(packages.TrueForAll(p => p.Destination.Port != 0), "Destination not valid/initialized");
            Assert.IsTrue(packages.TrueForAll(p => p.Destination.IpAddress != p.Source.IpAddress), "Source cannot be Destination Address");
        }

        [Test]
        public void generateFixedSourcePackageTest()
        {
            var source = new Address() { IpAddress = 123, Port = 21 };
            var packages = new List<Package>();

            for (int i = 0; i < 100; i++)
            {
                packages.Add(new Package(source, false));
            }

            Assert.IsTrue(packages.TrueForAll(p => p.Source.IpAddress == source.IpAddress), "Source should be fixed");
            Assert.IsTrue(packages.TrueForAll(p => p.Source.Port == source.Port), "Source should be fixed");
            Assert.IsTrue(packages.TrueForAll(p => p.Destination.IpAddress != 0), "Destination not valid/initialized");
            Assert.IsTrue(packages.TrueForAll(p => p.Destination.Port != 0), "Destination not valid/initialized");
            Assert.IsTrue(packages.TrueForAll(p => p.Destination.IpAddress != p.Source.IpAddress), "Source cannot be Destination Address");
        }

        [Test]
        public void generateFixedDestinationPackageTest()
        {
            var destination = new Address() { IpAddress = 123, Port = 21 };
            var packages = new List<Package>();

            for (int i = 0; i < 100; i++)
            {
                packages.Add(new Package(true, destination));
            }

            Assert.IsTrue(packages.TrueForAll(p => p.Source.IpAddress != 0), "Source not valid/initialized");
            Assert.IsTrue(packages.TrueForAll(p => p.Source.Port != 0), "Source not valid/initialized");
            Assert.IsTrue(packages.TrueForAll(p => p.Destination.IpAddress == destination.IpAddress), "Destination should be fixed");
            Assert.IsTrue(packages.TrueForAll(p => p.Destination.Port == destination.Port), "Destination should be fixed");
            Assert.IsTrue(packages.TrueForAll(p => p.Destination.IpAddress != p.Source.IpAddress), "Source cannot be Destination Address");
        }

        [Test]
        public void CreatePackagesTest()
        {
            var tubes = new List<Tube>()
            {
                new Tube(){ IpAddress = IpUtils.GetUInt32FromIPAddress("192.168.1.8") },
                new Tube(){ IpAddress = IpUtils.GetUInt32FromIPAddress("192.168.1.9") },
                new Tube(){ IpAddress = IpUtils.GetUInt32FromIPAddress("192.168.1.10") },
                new Tube(){ IpAddress = IpUtils.GetUInt32FromIPAddress("192.168.1.11") },
                new Tube(){ IpAddress = IpUtils.GetUInt32FromIPAddress("192.168.1.12") }
            };

            var odd_packages = Package.CreatePackages(3, tubes);

            //Produces odd numbers of packages
            Assert.AreEqual(9, odd_packages.Count);

            var even_packages = Package.CreatePackages(20, tubes);

            //for every Outgoing package an incoming answer ist generated
            Assert.IsTrue(even_packages.All(p1 => even_packages.Any(p2 => p1.Destination.IpAddress == p2.Source.IpAddress && p1.Source.IpAddress == p2.Destination.IpAddress)));

        }

    }
}
