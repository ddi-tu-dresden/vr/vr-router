using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptedRun : Run
{
    private int currentPackage = -1;
    private List<Package> packages = new List<Package>();

    public ScriptedRun(int tubeCount) : base(tubeCount)
    {
        this.Player = new Player() { Name = "DemoPlayer", Score = 0 };

        GenerateScriptedPackages();
        GenerateScriptedNatTable();
        NatTable.Entries = NatTable.Entries;
    }

    public override Package GenerateNextPackage(int difficulty)
    {
        throw new System.Exception("Runtime Generation of Packages not possible in scripted Runs!");
    }

    public Package GetNextScriptedPackage()
    {
        currentPackage++;

        Package nextPackage = null;

        if (packages.Count > currentPackage)
        {
            nextPackage = packages[currentPackage];
        }

        return nextPackage;
    }

    protected void GenerateScriptedNatTable()
    {
        NatTable.Entries = new List<NatTableEntry>()
        {
            new NatTableEntry()
            {
                ExternalAddress = new Address(IpUtils.GetUInt32FromIPAddress("142.67.42.5"), 6052),
                InternalAddress = new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.3"), 82)
            },

            new NatTableEntry()
            {
                ExternalAddress = new Address(IpUtils.GetUInt32FromIPAddress("142.67.42.5"), 6053),
                InternalAddress = new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.4"), 24)
            },

            new NatTableEntry()
            {
                ExternalAddress = new Address(IpUtils.GetUInt32FromIPAddress("142.67.42.5"), 6054),
                InternalAddress = new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.2"), 42)
            },

            new NatTableEntry()
            {
                ExternalAddress = new Address(IpUtils.GetUInt32FromIPAddress("142.67.42.5"), 6055),
                InternalAddress = new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.4"), 82)
            },

            new NatTableEntry()
            {
                ExternalAddress = new Address(IpUtils.GetUInt32FromIPAddress("142.67.42.5"), 6056),
                InternalAddress = new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.2"), 24)
            }
        };
    }

    protected void GenerateScriptedPackages()
    {
        packages.Clear();

        
        //1
        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.3"), 81),
                new Address(IpUtils.GetUInt32FromIPAddress("69.63.176.15"), 81)
            )
        );
        
        //2
        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.5"), 21),
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.2"), 21)
            )
        );
        
        //3
        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("69.63.176.15"), 82),
                new Address(IpUtils.GetUInt32FromIPAddress("142.67.42.5"), 6052)
            )
        );

        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("99.84.146.81"), 244),
                new Address(IpUtils.GetUInt32FromIPAddress("142.67.42.5"), 6053)
            )
        );

        //5
        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.2"), 5005),
                new Address(IpUtils.GetUInt32FromIPAddress("172.217.19.78"), 54)
            )
        );

        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.3"), 42),
                new Address(IpUtils.GetUInt32FromIPAddress("69.63.176.15"), 6500)
            )
        );

        //7
        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.5"), 21),
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.4"), 22)
            )
        );

        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("172.217.19.78"), 46),
                new Address(IpUtils.GetUInt32FromIPAddress("142.67.42.5"), 6054)
            )
        );

        //9
        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.4"), 28),
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.5"), 98)
            )
        );

        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("69.63.176.15"), 6054),
                new Address(IpUtils.GetUInt32FromIPAddress("142.67.42.5"), 6052)
            )
        );

        //11
        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.2"), 550),
                new Address(IpUtils.GetUInt32FromIPAddress("99.84.146.81"), 54)
            )
        );

        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.3"), 5607),
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.2"), 21)
            )
        );

        //13
        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.5"), 6001),
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.2"), 25)
            )
        );

        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("172.217.19.78"), 6003),
                new Address(IpUtils.GetUInt32FromIPAddress("142.67.42.5"), 6055)
            )
        );

        //15
        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("69.63.176.15"), 24),
                new Address(IpUtils.GetUInt32FromIPAddress("142.67.42.5"), 6056)
            )
        );

        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.3"), 98),
                new Address(IpUtils.GetUInt32FromIPAddress("99.84.146.81"), 54)
            )
        );

        //17
        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.2"), 24),
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.5"), 98)
            )
        );

        packages.Add(
            new Package(
                new Address(IpUtils.GetUInt32FromIPAddress("192.168.0.4"), 24),
                new Address(IpUtils.GetUInt32FromIPAddress("172.217.19.78"), 54)
            )
        );
    }


}
