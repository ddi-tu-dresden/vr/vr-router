﻿using System;

public class Address
{
    private UInt32 ipAddress;
    public UInt32 IpAddress
    {
        get => ipAddress;
        set
        {
            ipAddress = value;
            StrAddress = IpUtils.ConvertUIntToString(IpAddress);
        }
    }
    public UInt16 Port { get; set; }
    public string StrAddress { get; private set; }

    public Address(UInt32 address = 0, UInt16 port = 0)
    {
        IpAddress = address;
        Port = port;
    }

    public bool Equals(Address other)
    {
        return other.IpAddress == IpAddress &&
                other.Port == Port;

    }
}
