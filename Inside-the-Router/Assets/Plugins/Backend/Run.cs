﻿using System;
using System.Collections.Generic;
using System.Linq;
public class Run
{

    public static Run CurrentRun;
    public List<Tube> Tubes { get; protected set; }

    public Player Player { get; protected set; }

    public static event EventHandler<RunEventArgs> RunStarted;
    public static event EventHandler<RunEventArgs> CorrectPackagePlacement;
    public static event EventHandler<RunEventArgs> WrongPackagePlacement;
    public static event EventHandler<RunEventArgs> RunFinished;

    /// <summary>
    /// Do all the preparations for a run with mutable parameters
    /// After this constructor, the run should be ready to be started
    /// </summary>
    /// <param name="speed"></param>
    /// <param name="tubeCount"></param>
    /// <param name="RunLength"></param>
    public Run(int tubeCount)
    {
        Player = new Player() { Name = "DemoPlayer", Score = 0 };
        //Optional: IpUtils.ExternalRouterIP = ... ;
        Tubes = Tube.CreateTubes(tubeCount);
        NatTable.Entries = NatTable.GenerateRandomTableEntries(5, Tubes, IpUtils.ExternalRouterIP);
        SubscribeToAllTubes();
    }

    /// <summary>
    /// This starts the Run, e.g. spawns the first Package and so on...
    /// </summary>
    public void Start()
    {
        CurrentRun = this;
        RunStarted?.Invoke(this, new RunEventArgs("Run Started"));
    }

    public void End()
    {
        RunFinished?.Invoke(this, new RunEventArgs("Rund Finished"));
        CurrentRun = null;
    }

    protected void SubscribeToAllTubes()
    {
        foreach (var tube in Tubes)
        {
            tube.CorrectPackageReceived += OnCorrectPackageReceived;
            tube.WrongPackageReceived += OnWrongPackageReceived;
        }
    }

    private void OnWrongPackageReceived(object sender, RunEventArgs e)
    {
        Player.Score--;
        WrongPackagePlacement?.Invoke(sender, e);
    }

    private void OnCorrectPackageReceived(object sender, RunEventArgs e)
    {

        //Deactivated Dynamic NAT Table
        /*if (e.Type == "Correct Outgoing")
        {
            var p = e.Package as Package;
            if (NatTable.Entries.Any(o => o.InternalAddress.Equals(p.Source)))
            {
                var entry = NatTable.Entries.First(o => o.InternalAddress.Equals(p.Source));
                NatTable.CurrentEntries.Add(entry);
            }
        }*/

        CorrectPackagePlacement?.Invoke(sender, e);
    }

    protected Package GenerateRandomPackage() {
        return GenerateNextPackage(UnityEngine.Random.Range(1, 4));
    }

    public virtual Package GenerateNextPackage(int difficulty)
    {
        return difficulty switch
        {
            0 => GenerateRandomPackage(),
            1 => InitializeEasyPackage(),
            2 => InitializeMediumPackage(),
            3 => InitializeHardPackage(),
            _ => null
        };
    }

    private Package InitializeHardPackage()
    {
        var natEntry = NatTable.Entries[UnityEngine.Random.Range(0, NatTable.Entries.Count)];
        Address Source = IpUtils.GenerateRandomExternalAddress();
        Address Destination = natEntry.ExternalAddress;
        return new Package(Source, Destination);
    }

    private Package InitializeMediumPackage()
    {
        var client = Tubes[UnityEngine.Random.Range(0, Tubes.Count - 1)].IpAddress;
        var server = IpUtils.GenerateRandomExternalIP();
        var port = (ushort)UnityEngine.Random.Range(0, 300);

        var intern = new Address(client, port);
        var ext = new Address(server, port);
        Address Source = intern;
        Address Destination = ext;
        return new Package(Source, Destination);
    }

    private Package InitializeEasyPackage()
    {
        var port = (ushort)UnityEngine.Random.Range(21, 800);
        var source = Tubes[UnityEngine.Random.Range(0, Tubes.Count - 1)].IpAddress;
        var destination = source;
        while (destination == source) destination = Tubes[UnityEngine.Random.Range(0, Tubes.Count - 1)].IpAddress;

        Address Source = new Address(source, port);
        Address Destination = new Address(destination, port);
        return new Package(Source, Destination);
    }

}