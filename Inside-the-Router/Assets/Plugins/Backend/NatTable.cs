﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class NatTable
{
    public static List<NatTableEntry> Entries { get; set; } = new List<NatTableEntry>();

    /// <summary>
    /// Generates a Nat Table for a list of packages and Tubes.
    /// The method assumes, that the Packages are directly addressed and will alter
    /// the source and destination properties to match the generated NAT-Entries
    /// </summary>
    /// <param name="packages"></param>
    /// <param name="tubes"></param>
    /// <param name="gateway"></param>
    public static List<NatTableEntry> GenerateTableEntries(List<Package> packages, List<Tube> tubes, UInt32 gateway)
    {
        var entries = new List<NatTableEntry>();
        var clients = new List<UInt32>();
        var port = (ushort)6000;

        foreach (var t in tubes)
        {
            clients.Add(t.IpAddress);
        }

        foreach (var p in packages)
        {
            //Extern - Intern Package
            if (!clients.Any(c => p.Source.IpAddress == c) &&
                 clients.Any(c => p.Destination.IpAddress == c))
            {
                //Build translation Address
                var translation = new Address(gateway, port);
                port++;

                //Add NAT Entry
                entries.Add(new NatTableEntry()
                {
                    InternalAddress = p.Destination,
                    ExternalAddress = translation
                });

                //Change Package
                p.Destination = translation;
            }
        }

        return entries;
    }

    public static List<NatTableEntry> GenerateRandomTableEntries(int count, List<Tube> tubes, UInt32 gateway)
    {
        var entries = new List<NatTableEntry>();
        var clients = new List<UInt32>();
        var port = (ushort)6000;

        foreach (var t in tubes)
        {
            clients.Add(t.IpAddress);
        }

        for (int i = 0; i < count; i++)
        {
            //Build translation Address
            var translation = new Address(gateway, port);
            port++;

            //Add NAT Entry
            entries.Add(new NatTableEntry()
            {
                InternalAddress = new Address(clients[UnityEngine.Random.Range(0, clients.Count - 1)], (ushort)UnityEngine.Random.Range(21, 800)),
                ExternalAddress = translation
            });
        }

        return entries;
    }
}

