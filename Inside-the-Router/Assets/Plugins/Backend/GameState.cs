﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameState
{
    public float speed;
    public string difficulty;

    public static GameState CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<GameState>(jsonString);
    }
}
