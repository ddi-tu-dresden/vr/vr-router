﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Package
{
    public Address Source { get; set; }
    public Address Destination { get; set; }
    public object Content { get; set; }

    #region CONSTRUCTORS

    /// <summary>
    /// Use this constructor if you want to create a Package with random Source
    /// and Destination (internal or external)
    /// </summary>
    /// <param name="internalSource"></param>
    /// <param name="internalDestination"></param>
    public Package(bool internalSource, bool internalDestination)
    {
        UInt16 port = (UInt16)new System.Random().Next(20, 5000);

        if (internalSource) Source = new Address(IpUtils.CreateInternalIPAddress(), port);
        else Source = IpUtils.GenerateRandomExternalAddress();

        if (internalDestination) Destination = new Address(IpUtils.CreateInternalIPAddress(), port);
        else Destination = IpUtils.GenerateRandomExternalAddress();
    }

    /// <summary>
    /// Use this constructor if you want to create a Package with random Source
    /// (internal or external) and a fixed Destination
    /// </summary>
    /// <param name="internalSource"></param>
    /// <param name="givenDestination"></param>
    public Package(bool internalSource, Address givenDestination)
    {
        UInt16 port = (UInt16)new System.Random().Next(20, 5000);

        if (internalSource) Source = new Address(IpUtils.CreateInternalIPAddress(), port);
        else Source = IpUtils.GenerateRandomExternalAddress();

        Destination = givenDestination;
    }

    /// <summary>
    /// Use this constructor if you want to create a Package with random
    /// Destination (internal or external) and a fixed Source
    /// </summary>
    /// <param name="givenSource"></param>
    /// <param name="internalDestination"></param>
    public Package(Address givenSource, bool internalDestination)
    {
        UInt16 port = (UInt16)new System.Random().Next(20, 5000);

        Source = givenSource;

        if (internalDestination) Destination = new Address(IpUtils.CreateInternalIPAddress(), port);
        else Destination = IpUtils.GenerateRandomExternalAddress();
    }

    /// <summary>
    /// Use this constructor if you want a package for two given Addresses
    /// </summary>
    /// <param name="givenSource"></param>
    /// <param name="givenDestination"></param>
    public Package(Address givenSource, Address givenDestination)
    {
        Source = givenSource;
        Destination = givenDestination;
    }

    #endregion

    /// <summary>
    /// Create Packages, that will be addressed between the given tubes and random servers
    /// </summary>
    /// <param name="tripleCount"></param>
    /// <param name="tubes"></param>
    /// <param name="servers"></param>
    /// <returns></returns>
    public static List<Package> CreatePackages(int tripleCount, List<Tube> tubes)
    {
        if (tripleCount == 0 || tubes == null) throw new ArgumentException("Cannot create Packages with given Parameters.");

        var packages = new List<Package>();
        var rnd = new System.Random();

        //Intern - Intern Packages
        for (int i = 0; i < tripleCount; i++)
        {
            var port = (ushort)rnd.Next(21, 800);
            var source = tubes[rnd.Next(0, tubes.Count - 1)].IpAddress;
            var destination = source;

            while (destination == source)
            {
                destination = tubes[rnd.Next(0, tubes.Count - 1)].IpAddress;
            }

            packages.Add(new Package(new Address(source, port), new Address(destination, port)));
        }

        //Intern - Extern Packages
        //Extern - Intern Packages
        for (int i = 0; i < tripleCount; i++)
        {
            var client = tubes[rnd.Next(0, tubes.Count - 1)].IpAddress;
            var server = IpUtils.GenerateRandomExternalIP();
            var port = (ushort)rnd.Next(0, 300);

            var intern = new Address(client, port);
            var ext = new Address(server, port);

            packages.Add(new Package(intern, ext));
            packages.Add(new Package(ext, intern));
        }

        return packages;
    }

    public static List<Package> CreateEmptyPackages(int count)
    {
        var packages = new List<Package>();

        for (int i = 0; i < count; i++)
        {
            packages.Add(new Package(null, null));
        }

        return packages;
    }
}