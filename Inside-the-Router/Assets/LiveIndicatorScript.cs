using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using static UnityEngine.Mathf;

public class LiveIndicatorScript : MonoBehaviour
{
    private Transform Needle;
    private TextMeshProUGUI Text;

    private float CurrentSpeed;
    private float minSpeed = 0f;
    private float maxSpeed = 250f;
    private bool currentlyRunning = false;

    public float lossSpeed ;
    public float WrongPackagePlacement ;
    public float CorrectPackagePlacement ;

    //
    // Preparations
    //

    void Start()
    {
        Needle = transform.Find("Canvas/Needle");
        Text = transform.Find("Canvas/SpeedText").GetComponent<TextMeshProUGUI>();

        if (Needle == null) Debug.Log("Needle was not Found - Speed Indicator will not work!");
        if (Text == null) Debug.Log("Text was not Found - Speed Indicator will not work!");

        SubscribeToRunEvents();
    }

    private void OnDestroy()
    {
        UnsubscribeFromRunEvents();
    }

    private void SubscribeToRunEvents()
    {
        Run.RunStarted += OnRunStarted;
        Run.CorrectPackagePlacement += OnCorrectPackagePlacement;
        Run.WrongPackagePlacement += OnWrongPackagePlacement;
        Run.RunFinished += OnRunFinished;
    }

    private void UnsubscribeFromRunEvents()
    {
        Run.RunStarted -= OnRunStarted;
        Run.CorrectPackagePlacement -= OnCorrectPackagePlacement;
        Run.WrongPackagePlacement -= OnWrongPackagePlacement;
        Run.RunFinished -= OnRunFinished;
    }

    private void OnRunStarted(object sender, RunEventArgs e)
    {
        CurrentSpeed = 0;
        currentlyRunning = true;
    }

    private void OnRunFinished(object sender, RunEventArgs e)
    {
        currentlyRunning = false;

        string indicatorText = GenerateIndicatorText();
        Text.text = indicatorText;
    }

    private void OnWrongPackagePlacement(object sender, RunEventArgs e)
    {
        CurrentSpeed -= WrongPackagePlacement ;
    }

    private void OnCorrectPackagePlacement(object sender, RunEventArgs e)
    {
        CurrentSpeed += CorrectPackagePlacement ;
    }



    //
    // RunTime Functionality
    //

    void Update()
    {
        if (!currentlyRunning) return;
        
        CalculateNewSpeed();

        Vector3 rotation = CalculateNeedleRotation();
        Needle.localEulerAngles = rotation;

        string indicatorText = GenerateIndicatorText();
        Text.text = indicatorText;
    }

    private void CalculateNewSpeed()
    {
        CurrentSpeed -= lossSpeed * Time.deltaTime;
        CurrentSpeed = Clamp(CurrentSpeed, minSpeed, maxSpeed);
    }

    private string GenerateIndicatorText()
    {
        var value = "Speed: " + CurrentSpeed.ToString("00.0") + " Mbit/s";
        return value;
    }

    private Vector3 CalculateNeedleRotation()
    {
        Vector3 rot = new Vector3()
        {
            x = 0,
            y = 0,
            z = Lerp(80, -90, CurrentSpeed / maxSpeed)
        };

        return rot;
    }
}
