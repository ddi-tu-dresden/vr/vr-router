using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

public class MenuWallScript : MonoBehaviour
{
    public GameObject SelectorPackagePrefab;
    public Transform RespawnPoint;
    public AudioSource RespawnSound;

    public static bool RespawnDisabled = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }



    public void DisableRespawn()
    {
        RespawnDisabled = true;
    }

    public void EnableRespawn()
    {
        RespawnDisabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!RespawnDisabled)
        {
            Destroy(other.gameObject);
            Instantiate(SelectorPackagePrefab, RespawnPoint);
            RespawnSound.Play();
            StartCoroutine(DisableRespawnForSeconds(1f));
        }
    }

    IEnumerator DisableRespawnForSeconds(float f)
    {
        RespawnDisabled = true;

        yield return new WaitForSeconds(f);

        RespawnDisabled = false;
    }
}
